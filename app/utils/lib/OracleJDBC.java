package utils.lib;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import play.api.Logger;
 
public class OracleJDBC {
	private String user = "system";
	private String url = "jdbc:oracle:thin:@//localhost:1521/XE";
	private String driver = "oracle.jdbc.driver.OracleDriver";
	private String passw = "manager";
	private Connection connection = null;

	private static OracleJDBC instance = null;

	public static OracleJDBC getInstance() {
		if(instance == null) {
		 instance = new OracleJDBC();
		}
		return instance;
    }

	protected OracleJDBC(){

		try {
			connection = DriverManager.getConnection(url, user, passw); 
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
 
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
	}

	public Connection getConnection(){
		return connection;
	}
}