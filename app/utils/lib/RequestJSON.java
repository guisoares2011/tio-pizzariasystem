package utils.lib;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.StringWriter;
import org.json.simple.JSONObject;


public class RequestJSON {

	public static String ok(Object result) throws Exception {
		JSONObject response = new JSONObject();
        response.put("status", "OK");
        response.put("result", result);
        StringWriter out = new StringWriter();
        response.writeJSONString(out);
        String jsonText = out.toString();
        return jsonText;
	}
}