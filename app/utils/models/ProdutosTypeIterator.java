package utils.models;
import utils.models.exceptions.ProdutoNaoEncontrado;
import utils.lib.OracleJDBC;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class ProdutosTypeIterator extends CriteriaQuery {

    private static ProdutosTypeIterator instance = null;

    private ArrayList<ProdutosType> results;

    protected ProdutosTypeIterator() {}

    public static ProdutosTypeIterator getInstance() {
      if(instance == null) {
         instance = new ProdutosTypeIterator();
      }
      return instance;
    }

	public ArrayList<ProdutosType> getAll() throws SQLException{
		if(results == null){
			results = new ArrayList<ProdutosType>();
		}

		if(results.size() == 0){
			OracleJDBC oracle = OracleJDBC.getInstance();		
			Statement stmt = null;
			try{
				Connection c = oracle.getConnection();
				stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM ALL_TIPO_PRODUTOS");
				results = this.parseResultSet(rs);
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				if(stmt != null) {
					stmt.close();
				}
			}
		}
		return results;
	}

	public ArrayList<ProdutosType> parseResultSet(final ResultSet rs) throws SQLException{
		ArrayList<ProdutosType> result = new ArrayList<ProdutosType>();
		while(rs.next()){
			result.add(new ProdutosType(){{
				setId(rs.getInt("ID"));
				setType(rs.getString("TIPO"));
			}});
		}
		return result;
	}
}

