package utils.models;
import java.util.Map;
import java.util.Map.Entry;
import play.Logger;
import utils.models.exceptions.InvalidAttribute;
import utils.models.exceptions.InvalidQuery;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;



public class CriteriaQuery {

	public Utils helper = new Utils();

    public String table;

	public void createSelect(ArrayList<String> columns, ArrayList<String> tables, Map<String, Object> filter){
		String query = "SELECT " + 
						helper.join(", ", columns) + " FROM " + 
						helper.join(" ,", tables);
		if(filter.size() > 0) {
			try {
				ArrayList<String> filterReduced = this.reduceFilter(filter);
				query += " WHERE " + helper.join(" AND ", filterReduced);
			} catch(InvalidQuery e) {
				//Return Failed Select..
				System.out.println(e);
    			Logger.error(e.toString());
			}
		}
		Logger.info(query);
	}

    public String getTable(){
        return this.table;
    }

	public void update(ArrayList<String> columns, String table, Map<String, Object> filter){
		String query = "SELECT " + helper.join(", ", columns) + " FROM " + table;
		if(filter.size() > 0) {
			try {
				ArrayList<String> filterReduced = this.reduceFilter(filter);
				query += " WHERE " + helper.join(" AND ", filterReduced);
			} catch(InvalidQuery e) {
				//Return Failed Select..
				System.out.println(e);
    			Logger.error(e.toString());
			}
		}
		Logger.info(query);
	}

	private String createParam(String key, Map<String, Object> value) throws InvalidQuery{
		try {

			return makeConditionalParam(key, (String) value.get("conditional"), this.parseValueParam(value.get("value")));
		} catch(InvalidAttribute e){
			throw new InvalidQuery("Invalid params to query!");
		}
	}

	private String createParam(String key, String conditional, Object value)  throws InvalidQuery{
		try {

			return makeConditionalParam(key, conditional, this.parseValueParam(value));
		} catch(InvalidAttribute e){
			throw new InvalidQuery("Invalid params to query!");
		}
	}

    private String parseValueParam(Object value) throws InvalidAttribute {
        return this.parseValueParam(value, true);
    }

	private String parseValueParam(Object value, boolean putAspasInString) throws InvalidAttribute {

		if(value.getClass().isArray() || value instanceof ArrayList || value instanceof List){
			ArrayList value2 = (ArrayList) value;
			String result = "";	
			for(int i = 0, _len = value2.size(); i < _len; i ++){
				result += this.parseValueParam(value2.get(i), putAspasInString);
				if (i < _len - 1) result += " , ";
			}
			return result;

		} else if(helper.isString(value)){
            value = value.toString().replaceAll("'", "");
            if (putAspasInString) {
                value = "'" + value + "'";
            }
            return value.toString();
		} else if (helper.isInteger(value) || helper.isDouble(value)){
			return value.toString();
		} else {
			System.out.println("ERROR attribute:: " + value);
			throw new InvalidAttribute("Invalid attribute type");
    	}
	}

	private String makeConditionalParam(String key, String conditional, String value_parsed){
		String [] commonConditional = new String[] {">", "<","<>","<=",">=","="};
		
		if(Arrays.asList(commonConditional).contains(conditional)){
		
			return key + " " + conditional + " " + value_parsed;
		
		} else if(conditional.equals("like")){

			return key + " LIKE " + value_parsed;

		} else if(conditional.equals("not_like")){

			return key + " NOT LIKE " + value_parsed;

		} else if(conditional.equals("in")){

			return key + " IN (" + value_parsed + " )";
		} else {
			return "";
		}
	}

    public boolean insertNewRegister(Map<String, Object> data) {
        String query = "INSERT INTO " + table;
        try{
            String columns = this.parseValueParam(new ArrayList<>(Arrays.asList(data.keySet().toArray())), false);
            String values = this.parseValueParam(new ArrayList<Object>(Arrays.asList(data.values().toArray())), true);

            query += " ( " + columns + " ) VALUES (" + values + ");";
            System.out.println(query);
        } catch (InvalidAttribute e){
            return false;
        }
        return true;
    }

	private ArrayList<String> reduceFilter(Map<String, Object> filter) throws InvalidQuery{
		
		ArrayList<String> r = new ArrayList<String>();
		
		for (Entry<String, Object> entry : filter.entrySet())
		{
			String key = entry.getKey();
			Object value = entry.getValue();
			/*
				Da para simplicar na hora de montar a query se colocar direto o valor e ja considera como
				condicao =
			*/
			if(value instanceof Map){

				r.add(this.createParam(key, (Map<String, Object>)value));
			
			} else {
				r.add(this.createParam(key, "=", value));
			}
		}
		return r;
	}

    public boolean updateRegister(ObjectSQL objectSQL, Map<String, Object> data) {

        return true;
    }
}