package utils.models;
import play.data.DynamicForm;
import play.data.Form;
import forms.ProdutoForm;
import utils.lib.OracleJDBC;
import utils.models.exceptions.ProdutoNaoEncontrado;
import utils.models.exceptions.ProdutoNaoFoiRegistradoOuAlterado;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.sql.*;
import java.util.*;
import java.util.Map;
import java.lang.Integer;

public class ProdutosIterator extends CriteriaQuery {

    public String table = "produtos";
	//Singleton Pattern

    private static ProdutosIterator instance = null;

    protected ProdutosIterator() {}

    public static ProdutosIterator getInstance() {
      if(instance == null) {
         instance = new ProdutosIterator();
      }
      return instance;
    }

	public boolean delete(int id) throws SQLException{
		OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Produto> produtosResult = new ArrayList<Produto>();
		CallableStatement cs = null;
		try{
			Connection c = oracle.getConnection();
			cs = c.prepareCall("{ CALL DELETAR_PRODUTO(?, ?) }");
			// result, ID, ID_TIPO_PRODUTO, NOME, TIPO, VALOR
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setInt(2, id);
			cs.execute();
			if(cs.getInt(1) == 1){
				return true;
			} else {
				cs.close();
				return false;
			}
		} catch (Exception e){
			e.printStackTrace();
			if(cs != null) {
				cs.close();
			}
			return false;
		}
	}

	public Produto getProdutoById(int id) throws ProdutoNaoEncontrado, SQLException {
		OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Produto> produtosResult = new ArrayList<Produto>();
		CallableStatement cs = null;
		try{
			Connection c = oracle.getConnection();
			cs = c.prepareCall("{ CALL GET_PRODUTO_BY_ID(?, ?, ?, ?, ?, ?) }");
			// result, ID, ID_TIPO_PRODUTO, NOME, TIPO, VALOR
			cs.registerOutParameter(1, Types.INTEGER);
			cs.registerOutParameter(2, Types.INTEGER);
			cs.setInt(2, id);
			cs.registerOutParameter(3, Types.INTEGER);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, Types.NUMERIC);
			cs.execute();
			if(cs.getInt(1) == 1){
				Produto p = new Produto();
				p.setId(cs.getInt(2));
				p.setTipoID(cs.getInt(3));
				p.setNome(cs.getString(4));
				p.setTipo(cs.getString(5));
				p.setPreco(cs.getDouble(6));
				return p;
			} else {
				cs.close();
				throw new ProdutoNaoEncontrado("Produto nao pode ser encontrado");
			}
		} catch (SQLException e){
			e.printStackTrace();
			if(cs != null) {
				cs.close();
			}
			throw new ProdutoNaoEncontrado("Produto nao pode ser encontrado");
		}		
	}

    public int save(ProdutoForm form) throws SQLException, ProdutoNaoFoiRegistradoOuAlterado {
		OracleJDBC oracle = OracleJDBC.getInstance();		
		CallableStatement cs = null;
		try{
			Connection c = oracle.getConnection();
			cs = c.prepareCall("{ CALL CREATE_PRODUTO(?, ?, ?, ?, ?) }");
			
			//     RESULT OUT int,
			//     ID OUT PRODUTOS.ID%type,
			//     NOME IN PRODUTOS.NOME%TYPE,
			//     VALOR IN PRODUTOS.VALOR%TYPE,
			//     TIPO_ID IN PRODUTOS.ID_TIPO_PRODUTO%TYPE
			
			cs.registerOutParameter(1, Types.INTEGER);
			cs.registerOutParameter(2, Types.INTEGER);
			if(Integer.toString(form.id) != null){
				cs.setInt(2, form.id);
			}
			cs.setString(3, form.name.toString());
			cs.setDouble(4, form.price);
			cs.setInt(5, form.type);
			cs.execute();
			if(cs.getInt(1) == 1){	
				return cs.getInt(2);
			} else {
				cs.close();
				throw new ProdutoNaoFoiRegistradoOuAlterado("Produto nao pode ser registrado ou alterado");
			}
		} catch (SQLException e){
			e.printStackTrace();
			if(cs != null) {
				cs.close();
			}
			throw new ProdutoNaoFoiRegistradoOuAlterado("Produto nao pode ser registrado ou alterado" + e);
		}		
    }

    public ArrayList<Produto> getAll() throws SQLException{
		OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Produto> produtosResult = new ArrayList<Produto>();
		Statement stmt = null;
		try{
			Connection c = oracle.getConnection();
			stmt = c.createStatement();
			produtosResult = parseResultSet(stmt.executeQuery("SELECT * FROM ALL_PRODUTOS"));
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(stmt != null) {
				stmt.close();
			}
		}
		return produtosResult;
	}

	public ArrayList<Produto> getAllProdutoByIdPedido(int id) throws SQLException{
		OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Produto> produtosResult = new ArrayList<Produto>();
		Statement stmt = null;
		try{
			Connection c = oracle.getConnection();
			stmt = c.createStatement();
			produtosResult = parseResultSet(stmt.executeQuery("SELECT * FROM ALL_ITEM_PRODUTOS WHERE ID_PEDIDO = " + id));
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(stmt != null) {
				stmt.close();
			}
		}
		return produtosResult;
	}

	public ArrayList<Produto> parseResultSet(final ResultSet rs) throws SQLException{
		ArrayList<Produto> produtosResult = new ArrayList<Produto>();
		while(rs.next()){
			produtosResult.add(new Produto(){{
				setId(rs.getInt("ID"));
				setNome(rs.getString("NOME"));
				setTipo(rs.getString("TIPO"));
				setPreco(rs.getDouble("VALOR"));
				setTipoID(rs.getInt("ID_TIPO_PRODUTO"));
			}});
		}
		return produtosResult;
	}
}