package utils.models;
import java.io.StringWriter;
import org.json.simple.JSONObject;

/**
 * @Project - reactive-stocks
 * User: Guilherme
 * Date: 11/2/2014 - 6:45 PM
 * utils.models - <Description>
 */
public class ObjectSQL {

    int id;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    protected boolean registred = false;

    public boolean isRegistred(){
        return registred;
    }

    public void setRegistred(boolean registred){
        this.registred = registred;
    }

    public JSONObject asJSON (){
        JSONObject j = new JSONObject();
        j.put("id", getId());
        return j;
    }

    public String asJSONString(){
        try {
            StringWriter out = new StringWriter();
            asJSON().writeJSONString(out);
            return out.toString();
        } catch (Exception e) {
            return "{}";
        }
    }
}
