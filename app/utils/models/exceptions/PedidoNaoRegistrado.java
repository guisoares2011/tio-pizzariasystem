package utils.models.exceptions;

/**
 * @Project - MTP
 * User: Guilherme
 * Date: 4/5/14 - 5:35 PM
 * API.db.Exceptions - <Description>
 */
public class PedidoNaoRegistrado extends Exception {
    public PedidoNaoRegistrado(String message) {
        super(message);
    }
}
