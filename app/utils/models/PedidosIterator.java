package utils.models;
import play.data.DynamicForm;
import play.data.Form;
import forms.*;
import utils.lib.OracleJDBC;
import utils.models.exceptions.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.sql.*;
import java.util.*;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.util.Map;
import java.lang.Integer;

public class PedidosIterator extends CriteriaQuery {

    public String table = "produtos";

    public int funcionario_id = 1;
	//Singleton Pattern

    private static PedidosIterator instance = null;

    protected PedidosIterator() {}

    public static PedidosIterator getInstance() {
      if(instance == null) {
         instance = new PedidosIterator();
      }
      return instance;
    }

    public boolean changeStatus(MudaStatusForm data) throws SQLException, PedidoNaoRegistrado{
		OracleJDBC oracle = OracleJDBC.getInstance();		
		CallableStatement cs = null;
		try{
			Connection c = oracle.getConnection();
			cs = c.prepareCall("{ CALL MUDA_STATUS(?, ?, ?) }");
			/*
			  O_RESULT OUT INT,
			  P_ID OUT CLIENTE.ID%TYPE,
			  P_ID_STATUS OUT CLIENTE.ID%TYPE,
			*/
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setInt(2, data.id_pedido);
			cs.setInt(3, data.id_status);
			cs.execute();
			if(cs.getInt(1) == 1){	
				return true;
			} else {
				return false;
			}
		} catch (SQLException e){
			e.printStackTrace();
			if(cs != null) {
				cs.close();
			}
			throw new PedidoNaoRegistrado("Pedido nao pode ser alterado " + e);
		}	
    }

    public int save(PedidoForm form) throws SQLException, PedidoNaoRegistrado, ParseException {
		OracleJDBC oracle = OracleJDBC.getInstance();		
		CallableStatement cs = null;
		try{
			Connection c = oracle.getConnection();
			cs = c.prepareCall("{ CALL CREATE_CLIENTE(?, ?, ?, ?, ?, ?, ?) }");
			/*
			  O_RESULT OUT INT,
			  O_ID OUT CLIENTE.ID%TYPE,
			  P_NOME IN CLIENTE.NOME%TYPE,
			  P_TELEFONE IN CLIENTE.TELEFONE%TYPE,
			  P_RUA IN CLIENTE.RUA%TYPE,
			  P_CEP IN CLIENTE.CEP%TYPE,
			  P_BAIRRO IN CLIENTE.BAIRRO%TYPE
			*/
			cs.registerOutParameter(1, Types.INTEGER);
			cs.registerOutParameter(2, Types.INTEGER);
			cs.setString(3, form.nome);
			cs.setString(4, form.telefone);
			cs.setString(5, form.rua +", " + form.num + ", " + form.comp);
			cs.setString(6, form.cep);
			cs.setString(7, form.bairro);
			cs.execute();
			if(cs.getInt(1) == 1){	
				int id_cliente = cs.getInt(2);
				cs = c.prepareCall("{ CALL CREATE_PEDIDO(?, ?, ?, ?) }");
				  // RESULT OUT INT,
				  // O_ID OUT PEDIDO.ID%TYPE,
				  // P_ID_FUNCIONARIO IN PEDIDO.ID_FUNCIONARIO%TYPE,
				  // P_ID_CLIENTE IN PEDIDO.ID_CLIENTE%TYPE
				cs.registerOutParameter(1, Types.INTEGER);
				cs.registerOutParameter(2, Types.INTEGER);
				cs.setInt(3, this.funcionario_id);
				cs.setInt(4, id_cliente);
				cs.execute();
				if(cs.getInt(1) == 1){
					int id_pedido = cs.getInt(2);
					JSONParser parser = new JSONParser();
					JSONArray items_json = (JSONArray) parser.parse(form.items);

					for (int k = 0; k < items_json.size(); k++)
					{
						JSONObject item = (JSONObject) items_json.get(k);
						cs = c.prepareCall("{ CALL ADD_ITEM_PEDIDO(?, ?, ?, ?, ?) }");
						  // RESULT OUT INT,
						  // O_ID OUT ITEM_PEDIDO.ID%TYPE,
						  // P_ID_PEDIDO IN ITEM_PEDIDO.ID_PEDIDO%TYPE,
						  // P_ID_PRODUTO IN ITEM_PEDIDO.ID_PRODUTO%TYPE,
						  // P_QUANTIDADE IN ITEM_PEDIDO.QUANTIDADE%TYPE,
						  //   RESULT_MESSAGE OUT VARCHAR2(100)	
						cs.registerOutParameter(1, Types.INTEGER);
						cs.registerOutParameter(2, Types.INTEGER);
						cs.setInt(3, id_pedido);
						cs.setInt(4, Integer.parseInt(item.get("id").toString()));
						cs.setInt(5, Integer.parseInt(item.get("quantidade").toString()));
						cs.execute();
						if(cs.getInt(1) != 1){
							throw new PedidoNaoRegistrado("Nao foi possivel registrar o item");
						}
					}
					return id_pedido;
				} else{
					throw new PedidoNaoRegistrado("Nao foi possivel registrar o pedido");
				}
			} else {
				throw new PedidoNaoRegistrado("Nao foi possivel registrar o cliente");
			}
		} catch (SQLException e){
			e.printStackTrace();
			if(cs != null) {
				cs.close();
			}
			throw new PedidoNaoRegistrado("Pedido nao pode ser registrado" + e);
		}		
    }

    public Pedido getPedidoById(int id) throws SQLException{
    	OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Pedido> pedidosResult = new ArrayList<Pedido>();
		Statement stmt = null;
		try{
			Connection c = oracle.getConnection();
			stmt = c.createStatement();
			pedidosResult = parseResultSet(stmt.executeQuery("SELECT * FROM GET_ALL_PEDIDOS WHERE id= " + id));
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(stmt != null) {
				stmt.close();
			}
		}
		return pedidosResult.get(0);
    }

    public ArrayList<Pedido> getAll() throws SQLException{
		OracleJDBC oracle = OracleJDBC.getInstance();		
		ArrayList<Pedido> pedidosResult = new ArrayList<Pedido>();
		Statement stmt = null;
		try{
			Connection c = oracle.getConnection();
			stmt = c.createStatement();
			pedidosResult = parseResultSet(stmt.executeQuery("SELECT * FROM GET_ALL_PEDIDOS"));
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(stmt != null) {
				stmt.close();
			}
		}
		return pedidosResult;
	}

	public ArrayList<Pedido> parseResultSet(final ResultSet rs) throws SQLException{
		ArrayList<Pedido> pedidosResult = new ArrayList<Pedido>();
		while(rs.next()){
			pedidosResult.add(new Pedido(){{
				setId(rs.getInt("ID"));
				setNomeCliente(rs.getString("NOME_CLIENTE"));
				setData(rs.getString("DATA"));
				setStatusId(rs.getInt("ID_STATUS"));
				setStatus(rs.getString("STATUS"));
				setTelefone(rs.getString("TELEFONE"));
				setEndereco(rs.getString("ENDERECO"));
			}});
		}
		return pedidosResult;
	}

	public ArrayList<Status> getAllStatusList() throws SQLException{
		ArrayList<Status> statusList = new ArrayList<Status>();
		OracleJDBC oracle = OracleJDBC.getInstance();		
		Statement stmt = null;
		try{
			Connection c = oracle.getConnection();
			stmt = c.createStatement();
			final ResultSet rs = stmt.executeQuery("SELECT * FROM STATUS_PEDIDO");
			while(rs.next()){
				statusList.add(new Status(){{
					setId(rs.getInt("ID"));
					setValue(rs.getString("TIPO"));
				}});
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if(stmt != null) {
				stmt.close();
			}
		}
		return statusList;
	}
}

