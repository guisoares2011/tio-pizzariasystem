package utils.models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

@Entity 
public class Funcionario extends Model {

  @Id
  @Constraints.Min(10)
  public Long id;
  
  @Constraints.Required
  public String name;
  
  @Constraints.Required
  public String login;

  @Constraints.Required
  public String senha;
  
  @Formats.DateTime(pattern="dd/MM/yyyy")
  public Date dueDate = new Date();
  
  public static Finder<Long,Funcionario> find = new Finder<Long, Funcionario>(
    Long.class, Funcionario.class
  ); 

}