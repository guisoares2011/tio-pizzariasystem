package utils.models;
import java.util.ArrayList;
// import org.apache.commons.lang.math.NumberUtils;

public class Utils {


	public String join(String delimitor, ArrayList<String> list){
		int i = 0, size = list.size();
		String result = "";

		for(i = 0; i < size; i++){
			result += list.get(i);
			if(i < size - 1) result += delimitor;
		}
		return result;
	}

	// public boolean isNumber(Object s){
		// return NumberUtils.isNumber(s);;
	// }

	public boolean isDouble(Object s) {
    	try { 
        	Double.parseDouble(s.toString());
        	return true;		
    	} catch(NumberFormatException e) { 
        	return false; 
    	}
	}

	public boolean isString(Object o){
		return o.getClass().equals(String.class);
	}

	public boolean isInteger(Object s) {
    	try { 
        	Integer.parseInt(s.toString());
        	return true;		
    	} catch(NumberFormatException e) { 
        	return false; 
    	}
	}
}