package utils.models;

/**
 * @Project - reactive-stocks
 * User: Guilherme
 * Date: 11/2/2014 - 6:40 PM
 * utils.models - <Description>
 */
public class Cliente extends ObjectSQL{

    private String nome, telefone, rua, bairro, cep;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
