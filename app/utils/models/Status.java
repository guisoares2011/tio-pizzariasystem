package utils.models;

import java.lang.String;
import org.json.simple.JSONObject;

public class Status extends ObjectSQL {

	private String value;

	public String getValue(){
		return value;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String isChecked(int id){
		if(id == getId()){
			return " checked=\"checked\" ";
		} else {
			return "";
		}
	}
}