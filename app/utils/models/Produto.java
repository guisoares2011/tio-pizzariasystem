package utils.models;

import java.lang.String;
import org.json.simple.JSONObject;

public class Produto extends ObjectSQL {

	private double preco;
	private String nome;
    private String tipo;
    private int tipo_id, status_id;
    

	public double getPreco(){
		return preco;
	}

	public String getNome(){
		return nome;
	}

	public void setNome(String nome){
		this.nome = nome;
	}
	
	public void setPreco(double preco){
		this.preco = preco;
	}

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setTipoID(int tipo_id){
    	this.tipo_id = tipo_id;
    }


    public void setStatusId(int id){
    	this.status_id = id;
    }

    public int getTipoID(){
    	return this.tipo_id;
    }

    public int getStatusId(){
    	return this.status_id;
    }

	public JSONObject asJSON (){
		JSONObject j = super.asJSON();
		j.put("nome", getNome());
		j.put("tipo_id", getTipoID());
		j.put("tipo", getTipo());
		j.put("preco", getPreco());
		return j;
	}
}