package utils.models;

import java.util.ArrayList;

/**
 * @Project - reactive-stocks
 * User: Guilherme
 * Date: 11/2/2014 - 6:38 PM
 * utils.models - <Description>
 */
public class Pedido extends ObjectSQL {

    private String nome_cliente, data, status, endereco, telefone;
    private int status_id;

    public String getNomeCliente(){
        return nome_cliente;
    }


    public String getData(){
        return data;
    }

    public String getStatus(){
        return status;
    }

    public int getStatusId(){
        return status_id;
    }

    public String getEndereco(){
        return endereco;
    }

    public String getTelefone(){
        return telefone;
    }

    public void setNomeCliente(String nome_cliente){
        this.nome_cliente = nome_cliente;
    }

    public void setData(String data){
        this.data = data;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setStatusId(int status_id){
        this.status_id = status_id;
    }

    public void setEndereco(String endereco){
        this.endereco = endereco;
    }

    public void setTelefone(String telefone){
        this.telefone = telefone;
    }
}
