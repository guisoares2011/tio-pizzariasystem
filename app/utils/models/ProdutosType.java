package utils.models;

import java.lang.String;
import java.io.StringWriter;
import org.json.simple.JSONObject;

public class ProdutosType extends ObjectSQL {

	private String type;

	public String getType(){
		return this.type;
	}

	public void setType(String type){
		this.type = type;
	}

	public JSONObject asJSON (){
		JSONObject j = super.asJSON();
		j.put("type", getType());
		return j;
	}
}