class Pedidos

    URL_GET_ALL_PRODUTOS = '/produtos/all'
    URL_CADASTRO_PEDIDO = '/pedidos/novo'

    EMPTY_FUNCTION = ()->

    constructor: ()->
        @items = {}
        @item_size = 0
        @modal = null
        @product_list = []
        @createEventsInit()
        @createEventsForm()

    createEventsForm : ()->
        self = @
        $("#form-cliente-pedido").submit(()->
            data = {}
            $.each($(this).serializeArray(), (i, d)->
                data[d.name] = d.value
            )
            self.sendData(data)
            return false;
        )

    sendData : (data)->
        if @getTotalItems() > 0
            list = []
            for key, value of @getAllItems()
                list.push(value)

            data['items'] = JSON.stringify(list)
            $.post(URL_CADASTRO_PEDIDO, data, (response)->
                response = JSON.parse(response)
                if response.status == "OK"
                    bootbox.hideAll()
                    bootbox.alert("Pedido regristrado com sucesso!", ()->
                        window.location.href = "/pedidos"
                    )
                else
                    bootbox.alert(response.message)
            ).error(()->
                bootbox.alert("Aconteceu algum erro inesperado :|")
            )
        else
            bootbox.alert("Não é possivel cadastrar produtos, sem nenhum item.")

    getTotalItems : ()->
        total = 0
        for id, item of @getAllItems()
            total += item.quantidade
        return total

    getTotal : ()->
        total = 0
        for id, item of @getAllItems()
            total += item.preco * item.quantidade
        return total

    getItemId : (id)->
        _list = @getAllItems()
        return if id of _list then _list[id] else false

    hasItemId : (id)->
        return @getItemId(id) != false

    getAllItems : ()->
        return @items

    deleteItem : (id)->
        if @hasItemId(id)
            delete @item[id]
        #.. 
        @updateListItem()

    updateListItem : ()->
        self = @
        @updateTotalInfo()
        tbody = $("#list-produtos").find("tbody")
        tbody.find("tr").remove()
        for id, item of @getAllItems()
            tbody.append """
                <tr data-id="#{item.id}">
                    <td>#{item.id}</td>
                    <td>#{item.nome}</td>
                    <td><input type="number" class="quantidade-field input-sm form-control" value="#{item.quantidade}"></td>
                    <td>
                        <span>R$ #{item.preco}</span>
                        <a href="#" class="pull-right remove-data text-danger"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
            """
        tbody.find(".remove-data").click(()->
            id = parseInt($(this).parent().parent().attr("data-id"))
            self.deleteProductFromListViaId(id)
        )
        tbody.find(".quantidade-field").change(()->
            id = parseInt($(this).parent().parent().attr("data-id"))
            self.changeQuantidadeProduto(id, parseInt(this.value))
        )
        $("#list-produtos").dataTable();

    changeQuantidadeProduto : (id, value)->
        console.log(id, value)
        if @hasItemId(id)
            @items[id].quantidade = id
            @updateTotalInfo()

    deleteProductFromListViaId : (id)->
        if @hasItemId(id)
            delete @items[id]
            @updateListItem()

    updateTotalInfo : ()->
        $("#total-info").text("R$ #{@getTotal()}")

    createEventsInit : ()->
        self = @
        $("#addProduto").on("click", ()->
            self.addNovoItem()
        )

    addCurrentItemToList : ()->
        data = @getFormNovoItem()
        console.log(data)
        console.log(data, @hasItemId(data.id), (@hasItemId(data.id) != false))
        if(@hasItemId(data.id) == false)
            @items[data.id] = data
        console.log(@items)
        @updateListItem()
        @modal.modal("hide")

    getFormNovoItem : ()->
        indice = parseInt(@modal.find("select[name=product]").val())
        product = {}
        for i, p of @product_list
            if p.id == indice
                product = p
                break;

        return $.extend({}, p, {
            id : indice
            quantidade: parseInt(@modal.find("input[name=quantidade]").val())
        })

    getProdutosList : (fn = EMPTY_FUNCTION)->
        self = @
        $.get(URL_GET_ALL_PRODUTOS, (response)->
            fn.call(self, JSON.parse(response).result)
        )

    addNovoItem : ()->
        self = @
        @getProdutosList((list)->
            @modal = bootbox.dialog({
                "title": "Adicionar item" 
                message: """
                    <form role="form">
                        <div class="form-group">
                            <label for="product">Produto</label>
                            <select class="form-control" name="product">
                                <option>Selecione uma opção</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="quantidade">Quantidade</label>
                            <input type="text" name="quantidade" placeholder="Preço do Produto" class="form-control">
                        </div>
                    </form>
                """
                buttons:
                    success:
                        label: "Adicionar item"
                        className: "btn-warning"
                        callback: ()->
                            self.addCurrentItemToList()
                            return false
                    close:
                        label: "Cancelar"
                        className: "btn-default"
                        callback: ()->
                            self.modal.modal("hide")
            })
            @product_list = list
            select = @modal.find("select[name=product]")
            for i, item of list 
                select.append("""<option value="#{item.id}">#{item.id} - #{item.nome}</option>""")
        )

new Pedidos()