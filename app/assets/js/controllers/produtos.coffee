class @Produtos

    URL_GET_KIND_PRODUCTS = '/produtos/tiposProdutos'
    URL_SAVE_PRODUCT = '/produtos/save/'
    URL_DELETAR_PRODUTO = '/produtos/deletar/'

    ERROR_CADASTRO_PRODUTO = "Nao foi possivel cadastrar o produto!"
    ERROR_EDITAR_PRODUTO = "Aconteceu algum erro ao tentar salvar as alterações no produto."
    CADASTRO_PRODUTO_SUCCESS = "Cadastro realizado com sucesso"
    EDITAR_PRODUTO_SUCCESS = "Alterações realizadas com sucesso!"
    EDITAR_PRODUTO_SUCCESS = "Alterações realizadas com sucesso!"

    #It's same url but only post

    constructor : ()->
        #Initialize Events
        @createEventNewProduct()
        @createEventListProduct()
        @modal = null
        @data = null

    ###
        Creaate Events for new product
        When you click in + Novo Produto will open a modal with form to create a new product
    ###
    createEventNewProduct : ()->
        self = @
        $("#cadastrarNovo").click ()->
            self.createModal(self.createProductEvent)
            self.updateListTypeProducts()

    createEventListProduct : ()->
        self = @

        $('#list-produtos .edit-span').on("click", ()->
            data = JSON.parse(htmlspecialchars_decode($(this).parent().parent().attr("data-produto")))
            console.log(data)
            self.createModal(self.editProductEvent, data)
            self.updateListTypeProducts(()->
                self.createEventEditProduct(data)
            )
            return false
        )

    createEventEditProduct : (data) ->
        modal = @getModal()
        self = @
        modal.find("input[name=name]").val(data.nome)
        modal.find("select[name=type_product] option[value='#{data.tipo_id}']").prop('selected', true)
        modal.find("input[name=price]").val(data.preco)

        edit_link = modal.find(".delete-product")
        edit_link.show()
        edit_link.on("click", ()->
            bootbox.confirm("Deseja deletar este produto?", (result)->
               if result == true
                    self.deleteProduct(data.id)
                    bootbox.hideAll()
                    bootbox.dialog(
                        message: "Por favor aguarde.."
                        title: "Deletando um produto.."
                        buttons: {}
                        keyboard: false
                        backdrop: 'static'
                        closeButton: false
                    )

            )
        )

    getModal : ()->
        return @modal

    createProductEvent : ()->
        @clearErrorValidate()
        if @validateData() == true then @sendData() else @errorValidate()
        return false

    editProductEvent : (data)->
        @clearErrorValidate()
        if @validateData() == true then @sendData() else @errorValidate()
        return false

    createModal : (callback, data = null)->
        self = @
        @data = null
        @modal = bootbox.dialog({
            "title": if data then "Editar Produto : #{data.nome} - ##{data.id}"  else "Novo Produto" 
            message: """
                <form role="form">
                    <div class="form-group">
                        <label for="type_product">Nome do Produto</label>
                        <input type="text" placeholder="Nome do Produto" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="type_product">Tipo de Produto</label>
                        <select class="form-control" name="type_product">
                            <option>Selecione uma opção</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type_product">Preço</label>
                        <input type="text" name="price" placeholder="Preço do Produto" class="form-control">
                    </div>
                    <a href="javascript:void" style="displa:none" class="text-danger delete-product">
                        <i class="fa fa-times"></i> Deletar este produto.
                    </a>
                </form>
            """
            buttons:
                success:
                    label: if data then "Salvar Alterações" else "Cadastrar"
                    className: "btn-warning"
                    callback: ()->
                        callback.call(self, data)
                        return false
                close:
                    label: "Cancelar"
                    className: "btn-default"
                    callback: ()->
                        self.getModal().modal("hide")
        })
        @createFieldEvent(@modal)
        if data != null
            @data = data

    clearErrorValidate : ()->
        @getModal().find(".error").remove()

    errorValidate : ()->
        @getModal().find(".bootbox-body").append("""
            <p class="bg-danger error error-feedback-modal"><i class="fa fa-times"></i> Por favor preencha os dados corretamente.</p>
        """)

    createFieldEvent : (modal = @getModal())->

        #Mask Value input
        modal.find('input[name="price"]').mask("000000.00", {reverse: true, maxlenght: false})


    updateListTypeProducts : (fn = ()->)->

        type_product_list = @getModal().find("select[name=type_product]")
        type_product_list.empty()
        type_product_list.append("<option value=''>Selecione um tipo de Produto</option>")
        @getKindOfProduct (list)->
            for type in list
                type_product_list.append("<option value='#{type.id}'>#{type.type}</option>")
            fn.call(@)
        
        
    validateData : (data = @getDataForm())->
        return (data.name.length > 0 && (data.type != null || typeof data.type == "number") && (typeof data.price == "number" && data.price > 0))

    getKindOfProduct : (fn) ->
        self = @
        $.getJSON(URL_GET_KIND_PRODUCTS, (result) ->
            if result.status == "OK"
                console.log result, result.result
                fn.call(self, result.result)
            else
                bootbox.hideAll()
                bootbox.alert("Desculpe, serviço indisponivel no momento!")
        )

    getDataForm : ()->
        modal = @getModal()
        response =  {
            "name" : modal.find("input[name=name]").val()
            "type" : parseInt(modal.find("select[name=type_product]").val())
            "price" : parseFloat(modal.find("input[name=price]").val().replace(",", "."))
        }
        if @data != null && @data.id
            response.id = @data.id
        return response

    deleteProduct : (id)->
        self = @
        $.post(URL_DELETAR_PRODUTO + id, (response)->
            bootbox.hideAll()
            self.reportFeedback(if response == 'OK' then "Produto deletado com sucesso" else "Ocorreu um erro ao deletar o produto")
        ).error(
            self.reportFeedback("Ocorreu um erro ao deletar o produto")
        )

    sendData : ()->

        self = @
        data = @getDataForm()
        $.post(URL_SAVE_PRODUCT, data, (r)->
            r = JSON.parse(r)
            if r.status == 'ERROR_VALIDATION'
                bootbox.alert("Por favor, verifique se os dados foram preenchidos corretamente.")
            else
                if data.id != null
                    self.reportFeedback((if r.status == 'OK' then EDITAR_PRODUTO_SUCCESS else ERROR_EDITAR_PRODUTO))
                else
                    self.reportFeedback((if r.status == 'OK' then CADASTRO_PRODUTO_SUCCESS else ERROR_CADASTRO_PRODUTO))
        ).error(()->
            self.reportFeedback(ERROR_CADASTRO_PRODUTO)
        )

    reportFeedback : (message)->
        bootbox.hideAll()
        bootbox.alert(message, ()->
            window.location.reload()
        )

    window.Produtos = Produtos

new Produtos()

