package controllers;

import actors.*;
import akka.actor.*;
import akka.actor.ActorRef;
import play.libs.Akka;
import play.libs.F;

import java.util.List;
import java.util.Map;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Controller;
import forms.*;
import utils.lib.RequestJSON;
import utils.models.PedidosIterator;
import utils.models.ProdutosIterator;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.WebSocket;
import utils.BaseTemplateQuote;
import java.util.*;

/**
 * The main web controller that handles returning the index page, setting up a WebSocket, and watching a stock.
 * TODO: Arrumar layout da Novo Pedido para ter
 * Coluna 1 - INFORMAÇÕES DO CLIENTE | Coluna 2 - Lista de Items
 */
public class Pedidos extends Controller {

    public static Result index() {
        // ProdutoIterator Iproduto = new ProdutoIterator();
        // Iproduto.getProdutos
        try{
            return ok(views.html.pedidos.index.render(PedidosIterator.getInstance().getAll()));
        } catch(Exception e){
            return badRequest("Internal server ERROR " + e);
        }
    }

    public static Result novoPedido(){
        return ok(views.html.pedidos.cadastro.render()); 
    }

    public static Result cadastraPedidoNoBanco() {
        try {
            Form<PedidoForm> form = Form.form(PedidoForm.class).bindFromRequest();

            if (form.hasErrors()) {
                String errorsMsg = "There are " + form.errors().size() + " errors... \n";

                // Of course you can skip listing the errors
                for (Map.Entry<String, List<ValidationError>> entry : form.errors().entrySet()) {
                    String errorKey = entry.getKey();
                    List<ValidationError> errorsList = entry.getValue();
                    for (ValidationError validationError : errorsList) {
                        errorsMsg += errorKey + " / " + Messages.get(validationError.message()) + "\n";
                    }
                }

                return badRequest(errorsMsg);
            } else {
                PedidoForm data = form.get();
                return ok(RequestJSON.ok(PedidosIterator.getInstance().save(data)));
            }
        } catch(Exception e){
            e.printStackTrace();
            return badRequest("{status: 'ERROR', message: '" + e + "'}");
        }       
    }

    public static Result visualizarPedido(int id){
        /*
         * ID precisa ser String pq se nao passa do tamanho limite da variavel
        */
        //
        try{
            return ok(views.html.pedidos.visualizar.render(
                PedidosIterator.getInstance().getPedidoById(id), 
                ProdutosIterator.getInstance().getAllProdutoByIdPedido(id), 
                PedidosIterator.getInstance().getAllStatusList()));
        } catch(Exception e){
            return notFound("404");
        }
    }

    public static Result mudaStatusPedido(){
        try {
            Form<MudaStatusForm> form = Form.form(MudaStatusForm.class).bindFromRequest();

            if (form.hasErrors()) {
                String errorsMsg = "There are " + form.errors().size() + " errors... \n";

                // Of course you can skip listing the errors
                for (Map.Entry<String, List<ValidationError>> entry : form.errors().entrySet()) {
                    String errorKey = entry.getKey();
                    List<ValidationError> errorsList = entry.getValue();
                    for (ValidationError validationError : errorsList) {
                        errorsMsg += errorKey + " / " + Messages.get(validationError.message()) + "\n";
                    }
                }

                return badRequest(errorsMsg);
            } else {
                MudaStatusForm data = form.get();
                return ok(RequestJSON.ok(PedidosIterator.getInstance().changeStatus(data)));
            }
        } catch(Exception e){
            e.printStackTrace();
            return badRequest("{status: 'ERROR', message: '" + e + "'}");
        }  
    }
    public static Result done(){
        return ok("NAO IMPLEMENTADO");
    }
    public static Result cancelar(){
        return ok("NAO IMPLEMENTADO");
    }
}