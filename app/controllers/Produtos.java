package controllers;

import actors.*;
import akka.actor.*;
import akka.actor.ActorRef;
import play.libs.Akka;
import play.libs.F;
import java.util.*;
import play.data.DynamicForm;
import play.data.Form;
import forms.ProdutoForm;
import play.data.validation.ValidationError;
import play.mvc.Result;
import play.mvc.Controller;
import play.mvc.WebSocket;
import play.i18n.Messages;
import utils.BaseTemplateQuote;
import utils.lib.RequestJSON;
import utils.models.Produto;
import utils.models.ProdutosIterator;
import utils.models.ProdutosType;
import utils.models.ProdutosTypeIterator;


/**
 * The main web controller that handles returning the index page, setting up a WebSocket, and watching a stock.
 */
public class Produtos extends Controller {

    public static Result index() {
        try{

            return ok(views.html.produtos.index.render(ProdutosIterator.getInstance().getAll()));
        } catch(Exception e){
            return badRequest("Internal server ERROR " + e);
        }
    }

    public static Result get(int id){
        try {
            return ok(RequestJSON.ok(ProdutosIterator.getInstance().getProdutoById(id).asJSON()));
        } catch(Exception e){
            return badRequest("{status: 'ERROR', message: '" + e + "'}");
        }
    }

    public static Result all(){
        try{
            ArrayList list = new ArrayList();
            for(Produto p : ProdutosIterator.getInstance().getAll()){
                list.add(p.asJSON());
            }
            return ok(RequestJSON.ok(list));
        } catch(Exception e){
            return badRequest("{status: 'ERROR', message: '" + e + "'}");
        }
    }

    public static Result deletar(int id){
        try{

            boolean result = ProdutosIterator.getInstance().delete(id);
            if(result == true){
                return ok("OK");
            } else {
              return ok("ERROR");
            }
        } catch(Exception e){
            return ok("ERROR");
        }
    }

    public static Result save(){
        Form<ProdutoForm> form = Form.form(ProdutoForm.class).bindFromRequest();
        if (form.hasErrors()) {
            String errorsMsg = "There are " + form.errors().size() + " errors... \n";

            // Of course you can skip listing the errors
            for (Map.Entry<String, List<ValidationError>> entry : form.errors().entrySet()) {
                String errorKey = entry.getKey();
                List<ValidationError> errorsList = entry.getValue();
                for (ValidationError validationError : errorsList) {
                    errorsMsg += errorKey + " / " + Messages.get(validationError.message()) + "\n";
                }
            }

            return badRequest("{status: 'ERROR_VALIDATION'}");
        } else {

            try {
                ProdutosIterator p = ProdutosIterator.getInstance();
                // int id = p.save(form.get());
                return ok(RequestJSON.ok(p.save(form.get())));
                // return ok(RequestJSON.ok(ProdutosIterator.getInstance().getProdutoById(id).asJSON()));
            } catch(Exception e){
                e.printStackTrace();
                return badRequest("{status: 'ERROR', message: '" + e + "'}");
            }
        }
    }

    public static Result tiposDeProdutos(){
        try {
            final ProdutosTypeIterator p = ProdutosTypeIterator.getInstance();
            return ok(RequestJSON.ok(new ArrayList(){{
                for (ProdutosType type : p.getAll()){
                    add(type.asJSON());
                }
            }}));
        } catch(Exception e){
            e.printStackTrace();
            return badRequest("{status: 'ERROR', message: '" + e + "'}");
        }
    }

}
