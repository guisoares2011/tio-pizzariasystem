package controllers;

import actors.*;
import akka.actor.*;
import akka.actor.ActorRef;
import com.fasterxml.jackson.databind.JsonNode;
import com.avaje.ebean.*;
import play.libs.Akka;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.db.ebean.*;
import play.mvc.WebSocket;
import utils.models.Funcionario;
import utils.BaseTemplateQuote;
import javax.persistence.*;
import play.data.format.*;
import play.data.validation.*;
import scala.Option;
import utils.models.Produto;


/**
 * The main web controller that handles returning the index page, setting up a WebSocket, and watching a stock.
 */
public class Index extends Controller {

    public static Result index() {

        return ok(views.html.index.render());
    }

    public static Result getFuncionario(){
    	return ok("OI");
    }
}
