package forms;


import play.data.validation.Constraints;

public class ProdutoForm {
    //name=Teste+2&type=1&price=3.34
    @Constraints.Required()
    @Constraints.MinLength(3)
    public String name;

    @Constraints.Required()
    public int type;

    @Constraints.Required()
    public double price; 


    public int id;
    // @Constraints.Required()
    // public Long some_number;
 }

