package forms;


import play.data.validation.Constraints;

public class MudaStatusForm {
    //name=Teste+2&type=1&price=3.34
    @Constraints.Required()
    public int id_pedido;

    @Constraints.Required()
    public int id_status;
 }

