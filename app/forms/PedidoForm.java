package forms;
import java.util.*;


import play.data.validation.Constraints;

public class PedidoForm {

    @Constraints.Required()
    @Constraints.MinLength(3)
    public String nome;

    @Constraints.Required()
    @Constraints.MinLength(8)
    public String telefone;

    @Constraints.Required()
    public String rua;

    @Constraints.Required()
    public String num;

    public String comp;

    @Constraints.Required()
    public String cep;

    @Constraints.Required()
    public String bairro;

    public String items;

    // @Constraints.Required()
    // public Long some_number;
 }

